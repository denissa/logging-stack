##### The following requirements and recommendations apply when running Elasticsearch in Docker in production
```
sudo echo "vm.max_map_count=262144" | sudo tee /etc/sysctl.d/elasticsearch.conf
sudo sysctl -p /etc/sysctl.d/elasticsearch.conf
```

##### Create
```
docker-compose -f  docker-compose.setup.yml up setup_elasticsearch
docker-compose up -d elasticsearch
docker-compose -f  docker-compose.setup.yml up setup_kibana
docker-compose up -d kibana
```

##### Remove
```
docker-compose down
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker volume rm $(docker volume ls -f dangling=true -q)
```
##### Clean
```
sudo rm config/elasticsearch/elasticsearch.{crt,key,keystore}
sudo rm config/kibana/kibana.keystore
sudo rm -rf config/ssl/{ca,logging-stack,logging-stack-ca.zip,logging-stack.zip}
```
