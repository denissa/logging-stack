#!/bin/bash

if [ -f /config/elasticsearch/elasticsearch.keystore ]; then
    echo "Keystore already exists, exiting. If you want to re-run please delete config/elasticsearch/elasticsearch.keystore"
    exit 0
fi

# Determine if x-pack is enabled
echo "Determining if x-pack is installed..."
if [[ -f /usr/share/elasticsearch/bin/x-pack-env ]]; then
    if [[ -n "$ELASTIC_PASSWORD" ]]; then

        echo "=== CREATE Keystore ==="
        echo "Elastic password is: $ELASTIC_PASSWORD"
        if [ -f /config/elasticsearch/elasticsearch.keystore ]; then
            echo "Remove old elasticsearch.keystore"
            rm /config/elasticsearch/elasticsearch.keystore
        fi
        [[ -f /usr/share/elasticsearch/config/elasticsearch.keystore ]] || (/usr/share/elasticsearch/bin/elasticsearch-keystore create)
        echo "Setting bootstrap.password..."
        (echo "$ELASTIC_PASSWORD" | /usr/share/elasticsearch/bin/elasticsearch-keystore add -x 'bootstrap.password')
        mv /usr/share/elasticsearch/config/elasticsearch.keystore /config/elasticsearch/elasticsearch.keystore

        # Create SSL Certs
        echo "=== CREATE SSL CERTS ==="

        # check if old logging-stack-ca.zip exists, if it does remove and create a new one.
        if [ -f /config/ssl/logging-stack-ca.zip ]; then
            echo "Remove old ca zip..."
            rm /config/ssl/logging-stack-ca.zip
        fi
        echo "Creating logging-stack-ca.zip..."
        /usr/share/elasticsearch/bin/elasticsearch-certutil ca --pem --silent --out /config/ssl/logging-stack-ca.zip

        # check if ca directory exists, if does, remove then unzip new files
        if [ -d /config/ssl/ca ]; then
            echo "CA directory exists, removing..."
            rm -rf /config/ssl/ca
        fi
        echo "Install unzip if needed..."
        if ! command -v unzip &>/dev/null; then
             yum -y -q install unzip
        fi
        echo "Unzip ca files..."
        unzip /config/ssl/logging-stack-ca.zip -d /config/ssl

        # check if certs zip exist. If it does remove and create a new one.
        if [ -f /config/ssl/logging-stack.zip ]; then
            echo "Remove old logging-stack.zip zip..."
            rm /config/ssl/logging-stack.zip
        fi
        echo "Create cluster certs zipfile..."
        /usr/share/elasticsearch/bin/elasticsearch-certutil cert --silent --pem --in /config/ssl/instances.yml --out /config/ssl/logging-stack.zip --ca-cert /config/ssl/ca/ca.crt --ca-key /config/ssl/ca/ca.key

        if [ -d /config/ssl/logging-stack ]; then
            rm -rf /config/ssl/logging-stack
        fi
        echo "Unzipping cluster certs zipfile..."
        unzip /config/ssl/logging-stack.zip -d /config/ssl/logging-stack

        echo "Move elasticsearch certs to elasticsearch config dir..."
        mv /config/ssl/logging-stack/elasticsearch/* /config/elasticsearch/
    fi
fi
